*** Variables ***
${login address}    http://localhost:8078/webadmin/pages/common/login.jsp
${browser}        firefox

*** Keywords ***
Enter Username
    [Arguments]    ${username}
    Input Text    name=login    ${username}

Enter Password
    [Arguments]    ${password}
    Input Password    name=password    ${password}

Click the Login Button
    Click Button    xpath=//input[@class='button']

Click the Login Button And Don't Wait
    Click Button    xpath=//input[@class='button']    don't wait

Login Is Successful
    Page Should Contain Element    xpath=//div[@id='sidebar']

Login is Unsuccessful
    Page Should Contain Element    xpath=//*[@id='login']

Login Test Setup
    Open Browser    ${login address}    ${browser}
    Maximize Browser Window

Login Test Teardown
    Capture Screenshot
    Close Browser

Suite Startup Tasks
    [Documentation]    Common keyword for test suite startup tasks
    Start Selenium Server    -ensureCleanSession
    Set Selenium Timeout    10s
    Comment    -firefoxProfileTemplate

Suite Teardown Tasks
    [Documentation]    Common keyword for test teardown startup tasks
    Close All Browsers
    Stop Selenium Server

Testcase Teardown Tasks
    Close All Browsers

